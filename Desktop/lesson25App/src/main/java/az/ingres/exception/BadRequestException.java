package az.ingres.exception;

public class BadRequestException extends GenericError {

    public BadRequestException(ErrorCodes code, Object... args) {
        super(code.code, code.code, 400, args);
    }
}