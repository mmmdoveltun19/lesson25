package az.ingres.service;

import az.ingres.dto.UserDto;
import az.ingres.exception.BadRequestException;
import az.ingres.model.Authority;
import az.ingres.model.Role;
import az.ingres.model.User;
import az.ingres.repository.AuthorityRepository;
import az.ingres.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static az.ingres.exception.ErrorCodes.USERNAME_ALREADY_REGISTERED;
import static az.ingres.exception.ErrorCodes.USERNAME_NOT_FOUND;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService implements UserDetailsService {

    private final AuthorityRepository authorityRepository;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public void create(UserDto userDto) {
        Optional<User> byUserName = userRepository.findByUsername(userDto.getUsername());
        if (byUserName.isPresent()) {
            throw new BadRequestException(USERNAME_ALREADY_REGISTERED);
        } else {
            Authority userAuthority = authorityRepository.findByRole(Role.ADMIN);
            User build = User.builder()
                    .username(userDto.getUsername())
                    .password(bCryptPasswordEncoder.encode(userDto.getPassword()))
                    .authorities(List.of(userAuthority))
                    .build();
            userRepository.save(build);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new BadRequestException(USERNAME_NOT_FOUND));
        log.info("user is {}",user);
        return user;
    }
}