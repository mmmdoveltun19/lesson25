package az.ingres.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class Translator {

    private final MessageSource messageSource;

    public Optional<String> findByKey(String key, Locale lang, Object... arguments) {
        try {
            var res = messageSource.getMessage(key, arguments, lang);
            return Optional.of(res);
        } catch (NoSuchMessageException ex) {
            return Optional.empty();
        }
    }
}