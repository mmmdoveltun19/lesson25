package az.ingres.model;

public enum Role {
    USER,
    ADMIN
}
