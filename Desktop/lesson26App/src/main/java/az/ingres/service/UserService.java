package az.ingres.service;

import az.ingres.config.security.JwtService;
import az.ingres.dto.LoginRequestDto;
import az.ingres.dto.TokenResponseDto;
import az.ingres.dto.UserDto;
import az.ingres.exception.BadRequestException;
import az.ingres.model.Authority;
import az.ingres.model.Role;
import az.ingres.model.User;
import az.ingres.repository.AuthorityRepository;
import az.ingres.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static az.ingres.exception.ErrorCodes.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final AuthorityRepository authorityRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    public void create(UserDto userDto) {
        Optional<User> byUserName = userRepository.findByUsername(userDto.getUsername());
        if (byUserName.isPresent()) {
            throw new BadRequestException(USERNAME_ALREADY_REGISTERED);
        } else {
            Authority userAuthority = authorityRepository.findByRole(Role.ADMIN);
            User user = User.builder()
                    .username(userDto.getUsername())
                    .password(passwordEncoder.encode(userDto.getPassword()))
                    .authorities(List.of(userAuthority))
                    .build();
            userRepository.save(user);
        }
    }

    public TokenResponseDto login(LoginRequestDto dto) {
        User user = userRepository.findByUsername(dto.getUsername())
                .orElseThrow(() -> new BadRequestException(USERNAME_NOT_FOUND));
        if (passwordEncoder.matches(dto.getPassword(), user.getPassword())) {
            return jwtService.issueToken(user);
        }
        else {
            throw new BadRequestException(PASSWORD_MISMATCH);
        }
    }
}