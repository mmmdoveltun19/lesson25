package az.ingres.exception;

import az.ingres.service.Translator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.time.OffsetDateTime;
import java.util.Locale;

import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;

@RestControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    private final Translator translator;

    @ExceptionHandler(StudentNotFoundException.class)
    public final ResponseEntity<ErrorResponseDTO> handle(StudentNotFoundException ex, WebRequest request) {
        log.error("Student not found: {}");
        final ErrorResponseDTO response = ErrorResponseDTO
                .builder()
                .message("Not found.")
                .detail("Student with id {} not found")
                .code("MS19-ERROR-001")
                .status(404)
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
        return ResponseEntity.status(404).body(response);
    }

    @ExceptionHandler(BadRequestException.class)
    public final ResponseEntity<ErrorResponseDTO> handle(BadRequestException ex, WebRequest request) {
        var language = getLanguage(request.getHeader(ACCEPT_LANGUAGE));
        log.error("Bad request");
        final ErrorResponseDTO response = ErrorResponseDTO
                .builder()
                .message(translator.findByKey(ex.getCode(), new Locale(language),ex.getArguments()).get())
                .detail(translator.findByKey(ex.getCode().concat("_DETAILS"), new Locale(language),ex.getArguments()).get())
                .code(ex.getCode())
                .status(ex.getStatus())
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
        return ResponseEntity.status(ex.getStatus()).body(response);
    }

    private String getLanguage(String langHeader) {
        return ObjectUtils.isEmpty(langHeader) ? "az" : langHeader;
    }

}
