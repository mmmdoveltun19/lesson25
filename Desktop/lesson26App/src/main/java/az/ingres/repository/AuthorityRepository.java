package az.ingres.repository;

import az.ingres.model.Authority;
import az.ingres.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority,Long> {
    Authority findByRole(Role role);


}
