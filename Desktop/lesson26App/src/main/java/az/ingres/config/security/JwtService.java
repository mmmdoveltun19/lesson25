package az.ingres.config.security;

import az.ingres.dto.TokenResponseDto;
import az.ingres.model.Authority;
import az.ingres.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class JwtService {

    private static final String BEARER_AUTH_HEADER = "Bearer";
    private Key key;
    @Value("${security.jwtProperties.secret}")
    private String secretKey;

    @PostConstruct
    public void init() {
        byte[] keyBytes = Decoders.BASE64URL.decode(secretKey);
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public TokenResponseDto issueToken(User user) {
        var token = Jwts.builder()
                .setSubject(user.getPassword())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(15))))
                .signWith(key, SignatureAlgorithm.HS256)
                .claim("salam","sagol")
                .claim("role",user.getAuthorities().stream().map(Authority::getRole).toString())
                .compact();
        return new TokenResponseDto(token);
    }

    public Optional<? extends Authentication> getAuthentication(HttpServletRequest request) {
        //headrdan tokeni goturmek
        String authorization = request.getHeader("Authorization");
        if (authorization == null) {
            return Optional.empty();
        }
        var token = authorization.substring(BEARER_AUTH_HEADER.length()).trim();
        //tokenin valid olmasi yoxlamag

        Jws<Claims> claimsJws = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token);

        Claims body = claimsJws.getBody();
        if (body.getExpiration().before(new Date())) {
            throw new IllegalArgumentException("Token expired");
        }

        return Optional.of(getAuthenticationBers(body));

    }
    //authentication qaytarmag

    private Authentication getAuthenticationBers(Claims claims) {
        List<String> roles = claims.get("role",List.class);
        var authorityList = roles
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.toString()))
                .collect(Collectors.toList());

        var details = new org.springframework.security.core.userdetails.User(
                claims.getSubject(),
                "",
                authorityList
        );

        return new UsernamePasswordAuthenticationToken("",details,authorityList);

    }
}