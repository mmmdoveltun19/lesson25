package az.ingres.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class HelloController {

    @GetMapping("/public")
    public String hello() {
        return "Hello";
    }

    //user & admin
    @GetMapping("/user")
    public String user() {
        return "Hello User";
    }

    //admin
    @GetMapping("/admin")
    public String admin() {
        return "Hello Admin";
    }
}