package az.ingres;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@Slf4j
@EnableCaching
public class Lesson26App implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Lesson26App.class, args);

	}

	//	@Override
	public void run(String... args) throws Exception {
//        List<Student> a = new ArrayList<>();
//        for (int i = 0; i < 10000; i++) {
//            a.add(Student.builder()
//                            .id(UUID.randomUUID())
//                    .name("Ali " + i)
//                    .lastname("Mammadov " + i)
//                            .age(i)
//                    .build());
//        }
//        studentRepository.saveAll(a);
//        System.out.println("done");
	}
}
